# Hybrid-Veranstaltungen

Was sind hybride Seminare? Bei einem Hybrid-Seminar handelt es sich eine Mischform aus Präsenz- und Online-Seminar. Da Teilnehmer:innen sowohl vor Ort als auch online teilnehmen, müssen die Bedürfnisse beider Gruppen berücksichtigt werden, was die technische Umsetzung deutlich aufwendiger macht. Die besondere Herausforderung ist, dass beide Gruppen gleichberechtigt in Interaktion kommen. Auf Grund der hohen technischen Hürden ist es dringend empfehlenswert vorher abzuwägen, ob eine hybride Veranstaltung überhaupt Vorteile hat gegenüber einer reinen Online-Veranstaltung.

## Technische Mindestvoraussetzungen

Hybride Seminare sind technisch aufwendig. Punkt. Als (absolute) Mindestvoraussetzung sind folgende Geräte notwendig:

- Beamer + Leinwand + Lautsprecher: damit die Teilnehmer:innen die Online-Gruppe gut sehen und verstehen können.
- mindestens 2 Kameras + 2 Mikrofone (Plenum + Referent:in): damit die Online-Teilnehmer:innen das Geschehen vor Ort gut sehen und verstehen können.
- Leistungsfähiges Notebook, bzw. Rechner, da das Zusammenführen von je 2 Audio- und Videostreams relativ rechenintensiv ist.

## Aufbau der Technik

Damit alle Teilnehmer:innen sowohl online als auch vor Ort das Geschehen gut verfolgen können, empfiehlt sich ein Aufbau nach folgendem Schema:

![hybrid.png](assets/hybrid.png)

*Technischer Aufbau bei einer hybriden Veranstaltung. Darstellung: CC BY-SA Christian Pfliegel*

## Rollen in einem hybriden Seminar

Wie bei einem Präsenz- oder Online-Seminar sind auch bei einem hybriden Seminar verschiedene Rollen zu besetzen. Da die Zielgruppe sowohl in Präsenz als auch online vertreten ist, ist eine gute Aufteilung vorher noch wichtiger als in einem reinen Online-Format.

**Folgende Rollen sollten vergeben werden:**

- **Moderator:in:** Die Person, die die Moderation übernimmt, hat zwei Aufgaben: durch das Seminar führen und die Reihenfolge von Beiträgen festlegen. Der oder die Moderator:in muss dringend vor Ort sein, um die Präsenzteilnehmer:innen aufzurufen oder die Online-Teilnehmer:innen. Wichtig ist dabei, dass der Chat ständig beobachtet wird, sodass sich die Online-Teilnehmer:innen auch zu Wort melden können. Tipp: Vereinbaren, dass ein Sternchen in den Chat geschrieben als Meldung gilt.
- **Technik:** Ein hybrides Seminar ist technisch deutlich aufwendiger, weshalb dringend zu empfehlen ist, dass eine Person nur für die Technik zuständig ist. Aufgaben u. a. sind: Umschalten der Kameras, Mikrofone, Lautstärke steuern, Ansprechpartner:in für die Online-Teilnehmer:innen, Einrichtung von Arbeitsgruppen, Bereitstellung von Links usw.
- **Online-Moderation:** Diese Person nimmt aus der Ferne im Online-Raum teil und ist die Schnittstelle zwischen den Online-Teilnehmer:innen und dem Organisationsteam: sie meldet direkt technische Probleme (Sound, Bild, Verbindungsstörungen), zudem begrüßt sie die Teilnehmer:innen bei der Ankunft im Online-Seminar und steht dort als Ansprechpartner:in zur Verfügung.

> **Praxistipp:** Die Online-Moderator:in ist eine sehr wichtige Rolle, die unbedingt besetzt werden sollte. Für die Personen vor Ort ist es sehr schwierig, die Online-Teilnehmer:innen angemessen begrüßen zu können, da ja die Präsenz-Teilnehmer:innen zur gleichen Zeit ankommen, was die Lautstärke im Raum deutlich erhöht.
Eine gemeinsame digitale Plattform für beide Gruppen (neben des Chats) ist empfehlenswert, sodass gemeinsam Ergebnisse gesichert werden können oder dokumentiert werden kann. Diese Technik am Anfang gut einführen!
Am Anfang lieber größere Arbeitsgruppen einrichten, damit sich die Teilnehmer:innen an das neue Format gewöhnen können!

## Formen von hybriden Veranstaltungen

Hybride Formate sind in verschiedenen Versionen und Abstufungen durchführbar:

In einer einfacheren Form ist es möglich, dass nur das Team und die Referent:innen vor Ort sind, während alle Teilnehmer:innen nur online zugeschaltet sind.

In einer anderen Form sind die Teilnehmer:innen vor Ort, aber die Referent:innen werden zugeschaltet (beispielsweise dann, wenn die Referent:innen eine zu weite Anreise hätten).

Aber auch andere „gemischte“  Formen sind möglich:

Ein Teil der Teilnehmer:innen ist vor Ort, ein anderer Teil ist online zugeschaltet. Gründe können auch hier eine zu weite Anreise, ein zu kleiner Raum oder andere Einschränkungen sein.

Verschiedene Gruppen treffen sich in lokalen Präsenzgruppen, die dann online miteinander verbunden sind (Hybride „Satelliten-Veranstaltung).

Die Aufzählung dieser unterschiedlichen Formate zeigt, dass hybride Veranstaltungen sehr vielfältig und spannend sein können. Vor allem die Einbindung internationaler Referent:innen und Teilnehmer:innen eröffnet ganz neue Möglichkeiten, die eine große Bereicherung für alle Beteiligten ist. Auch hybride Satelliten-Veranstaltungen können ganz neue Möglichkeiten schaffen.

Bei der Planung und Konzeption der Veranstaltung sind dabei ganz dringend vor allem zwei Punkte zu beachten:

Die Technik muss reibungslos funktionieren, sonst wird es für für alle sehr schnell sehr frustrierend. Vor allem ein guter Ton kann wirklich eine große Herausforderung sein, gleichzeitig ist dieser existenziell für das Gelingen. Am besten schon rechtzeitig vor der Veranstaltung einen (Sound-)Check durchführen, da ggfs. noch nachgebessert werden muss.

Konzeptionell gut überlegen, wie die Teilnehmer:innen gut eingebunden werden können. Die größte Gefahr liegt darin, dass die Online-Teilnehmer:innen zur zweiten Klasse deklassiert werden, z. B. indem sie nicht genügend interaktiv eingebunden oder „übersehen“ werden.

> **Praxistipp und Warnung:** Hybride Veranstaltungen sind deutlich aufwendiger als reine Online-Veranstaltungen. Daher vorher gut abwägen, ob das hybride Format wirklich einen Vorteil für die Teilnehmer:innen mit sich bringt oder ob nicht doch eine reine Online-Veranstaltung die bessere Wahl wäre!