# Vorwort

## Lizenz 
Dieses Werk ist lizenziert unter einer Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 international Lizenz (CC BY-SA). 
Weitere Informationen finden Sie unter https://creativecommons.org/licenses/by-sa/4.0/deed.de

Materialien aus anderen Quellen sind klar kenntlich gemacht und unterliegen unter Umständen einer anderen Lizenz! Insbesondere Logos sind nicht unter einer offenen Lizenz freigegeben. 

Anmerkung zu Kapitel 2: Der Inhalt dieses Abschnitts basiert auf dem Text „Tipps und Tricks für Online Seminare - eFeF Fortbildungsreihe, der von Clémence Bosselut und Frederike von Geisau für eFeF verfasst wurde und von Christian Pfliegel für die Bildungsarbeit von Mission EineWelt angepasst und erweitert wurde. 

Der ursprüngliche Text steht ebenfalls unter einer offenen Lizenz (CC BY-SA Bosselut, von Geisau, Pfliegel). In Teilen wurden die Gliederung übernommen, die Inhalte wurden zwischen Januar 2021 und März 2021 für diesen Abschnitt stark verändert, erweitert und aktualisiert.

Kapitel *Eine Schülerzeitung geht online* wurde von Miriam Zöllich und Christian Pfliegel verfasst und soll bei Wiederverwendung wie folgt genannt werden: 
CC BY-SA 4.0 Miriam Zöllich und Christian Pfliegel

Kapitel *OER* ist ein Remix aus der Präsentation „OER in der Hochschule“, die von Julia Joachim erstellt wurde und unter einer CC BY-SA 4.0 - Lizenz steht. Der Link zur Originalquelle findet sich im Anhang. 

Eine Übersicht der verwendeten Quellen findet sich am Ende des Buchs. Die Lizenz soll bei Weiterverwendung wie folgt genannt werden: 
CC BY-SA 4.0 Christian Pfliegel.

## Zielgruppe
Dieses Handbuch richtet sich an Menschen, die bisher wenige Erfahrungen mit der Durchführung und Konzeption digitaler Bildungsformate sammeln konnten. Ich hoffe aber, dass auch für Erfahrenere der ein oder andere praktische Tipp enthalten ist.

## Über mich und dieses Handbuch
Nicht erst seit meinem Master an der FernUniversität in Hagen im Studiengang eEducation: Bildung und Medien beschäftige ich mich mit den Chancen, aber auch mit den Grenzen digitaler Bildung. 
Seit März 2020 hat dieser Bereich Corona-bedingt einen ziemlichen Schub erfahren. In dieser Zeit habe ich im Rahmen meiner Arbeit bei Mission EineWelt in Neuendettelsau, wo ich als E-Learning-Entwickler arbeite, ca. 200 Zoom-Konferenzen als technischer Support betreut und habe ca. 15 Großveranstaltungen von Präsenz auf Online neu konzipiert. Die Erfahrungen dieser Monate sind in diesem Handbuch niedergeschrieben. Da sie für manche da draußen bestimmt eine gute Hilfe sein können, darf und soll das Handbuch gemäß Creative Commons Lizenz frei geteilt werden.

## Vorbemerkungen zur verwendeten Software 
Der Text ist, wo möglich, allgemein geschrieben und mit verschiedener Software einsetzbar. Ziel des vorliegenden Textes ist es, Methoden zu beschreiben und keine Software-Anleitung zu geben. Ich selbst verwende derzeit für Online-Seminare in der Regel die Software Zoom.
An der ein oder anderen Stelle gebe ich konkrete Beschreibungen, wie das Vorgehen konkret in Zoom ist (z. B. die Breakouträume). Bei anderen Lösungen ist häufig ein analoges Vorgehen möglich. Hierzu bitte einfach die Dokumentation eures Videokonferenzsystems lesen!