# Computervermittelte Kommunikation (CvK)

## Grundlagen: Theorie & Forschung

Während die bisherigen Kapitel sehr praxisorientiert waren, soll das nun folgende letzte und abschließende Kapitel einen Einblick in die Theorie von digitaler Kommunikation geben. Für manche:n mag dies etwas trocken erscheinen. Ich denke jedoch, dass dieser Ausflug in die Theorie helfen kann, bestimmte Dinge und Probleme, die bei digitalen Online-Veranstaltungen auftreten können, besser zu verstehen. Grundlage dieses Abschnitts ist ein Text, den ich im Rahmen meines Masterstudiums an der FernUniversität in Hagen erstellt habe.

## Der Kommunikationsbegriff

Der deutsche Begriff *Kommunikation* ist abgeleitet vom lateinischen Ausdruck communicatio, der mit *Mitteilung* übersetzt werden kann.

Je nach Forschungsdisziplin wird Kommunikation verschieden aufgefasst: Die Informationstechnik definiert sie als Informationsaustausch zwischen Sender:in und einem Empfänger:in mittels bestimmter Zeichen und Codes (Schäfers 1998:176). Der handlungstheoretische Kommunikationsbegriff beschreibt Prozesse, „in denen sich Individuen als denkende, sprechende, empfindende und handelnde Personen zueinander in Beziehung setzen“ (ebenda). Die Systemtheorie wiederum fasst Kommunikation auf als Verknüpfungen von Ereignissen innerhalb sozialer Systeme sowie zwischen Systemen und ihrer Umwelt, hergestellt durch Sprache oder generalisierte Kommunikationsmedien (ebenda). Diesen Definitionen gemeinsam ist zum einen die Tatsache, dass mindestens zwei Personen benötigt werden, damit Kommunikation überhaupt stattfinden kann. Des Weiteren sind nach Six (2007) sechs Bestimmungsstücke in Kommunikation typischerweise feststellbar, dargestellt in folgender Abbildung:

![kommunikation.jpeg](assets/kommunikation.jpeg)

*Abbildung: Bestimmungsstücke von Kommunikation. Quelle: Six u. a. 2007. Eigene Darstellung. CC BY-SA Christian Pfliegel*

Im Lauf der Geschichte der Menschheit war Kommunikation immer wieder Wandlungen unterworfen: Ursprünglich war nur face-to-face-Kommunikation möglich, seit Beginn der Industrialisierung wurde diese Form zunehmend durch  technische Hilfsmittel gleichzeitig ergänzt und abgelöst. Durch Corona hat die Digitalisierung der Kommunikation während der letzten Monate noch stark an Bedeutung gewonnen. Dabei ist noch nicht absehbar, welchen dauerhaften Einfluss diese Entwicklung in der Zeit nach Corona haben wird.

Kommunikationsmedien veränderten die Art und Weise, wie sich die Menschen mitteilen. Buch, Zeitung, TV und Radio waren reine Überträger von Informationen, die Kommunikation nur in eine Richtung ermöglichten. Ein Wandel setzte mit der Erfindung des Telefons ein, die Neuen Medien schließlich veränderten die Art und Qualität der Kommunikation von Grund auf. Auch der Bereich Bildung ist hiervon nicht ausgenommen, in allen Schulformen, angefangen von der Grundschule bis hin zu den Hochschulen, war der Einfluss schon lange vor Lockdown und Homeschooling massiv: Online-Unterricht, der Austausch mit Lehr- und Lernpartner:innen mit Hilfe sozialer Netzwerke oder das Nachschlagen von Faktenwissen innerhalb kürzester Zeit an jedem Ort der Welt sind nur einige Beispiel hierfür.

Im Gegensatz zu traditioneller Kommunikation läuft die Kommunikation hierbei weniger face-to-face ab, zumeist läuft sie via Computer. Der enorme Wachstum des Internets, und damit der Bedeutungszuwachs von computervermittelter Kommunikation wird in folgender Abbildung dargestellt:

![internet.jpeg](assets/internet.jpeg)

Abbildung: *Anzahl registrierter Internet-Domains. Quelle: Internet Systems Consortium (2017). Diese Abbildung steht nicht unter einer freien Lizenz!*

Das vorliegende Kapitel hat das Ziel, die Besonderheiten computervermittelter Kommunikation (CvK) auszuarbeiten und den Einsatz von Bildungstechnologie zu diskutieren.

## Kommunikation: Theorien und Modelle

Hierfür ist es zunächst notwendig zu definieren, was im vorliegenden Text unter Kommunikation und unter CvK verstanden werden soll. In der Fachliteratur findet sich zu beiden keine einheitliche Definition.

Um die Besonderheiten von CvK diskutieren zu können ist es notwendig, sich zunächst mit dem Begriff Kommunikation auseinanderzusetzen. Hierfür sollen verschiedene Theorien unterschiedlicher Autor:innen vorgestellt werden. Im Anschluss geht es um die Besonderheiten von CvK als Sonderfall von Kommunikation. Der Fokus soll dabei immer auf die Bildungswissenschaft und auf Bildungsveranstaltungen gelegt werden.

Wie bereits in der Einleitung dargestellt, wird der Begriff Kommunikation je nach Wissenschaftsdisziplin verschieden definiert. Es gibt zahlreiche Modelle, die Kommunikation je nach Ausrichtung verschieden beschreiben. Krauss und Fussel (1996) fassen diese verschiedenen Modelle in insgesamt vier Gruppen zusammen. Jeweils dargestellt mit den wichtigsten Vertretern werden sie in folgender Abbildung:

![modelle.jpeg](assets/modelle.jpeg)

Abbildung: *Kommunikationsmodelle, gruppiert nach Krauss und Fussel (1996). Eigene Darstellung. CC BY-SA Christian Pfliegel*

In die Gruppe **Encoder - Decoder - Modelle** fallen alle Kommunikationsmodelle, die ihr Hauptaugenmerk auf die Verschlüsselung (=Enkodierung), Übertragung und Entschlüsselung (=Dekodierung) von Botschaften legen. Die wichtigste Frage bei diesen Modellen ist, wie Botschaften optimal übertragen werden können, ohne dass Störungen und Probleme auftreten, die den reibungslosen Kommunikationsablauf stören (Röhner & Schütz, 2012, S. 16).

**Intentionale Modelle** gehen vor allem der Frage nach, wie Kommunikation gelingen kann. Sie beziehen sich auf die Absicht des Kommunizierenden, dem Rezipierenden das „Gemeinte“ zu übermitteln (Röhner & Schütz, 2012, S. 16).

**Modelle der Perspektivenübernahme** gehen der Frage nach, wie Menschen sich besser verstehen können (ebenda). Da diese Modelle sehr psychologisch ausgerichtet sind, soll auf sie an dieser Stelle nicht weiter eingegangen werden. Der bekannteste Vertreter der sog. **Dialog-Modelle** ist Paul Watzlawick. Diese gehen vor allem der Frage nach, wie durch Kommunikation Wirklichkeit konstruiert wird (ebenda).

In den folgenden Unterkapiteln sollen nun die Theorien und Modelle näher vorgestellt werden, die für ein besseres Verständnis der Besonderheiten computervermittelter Kommunikation eine wichtige Rolle spielen.

### Das Modell von Shannon und Weaver

Das Kommunikationsmodell von Shannon und Weaver bildet die Grundlage vieler Definitionen von Kommunikation. Das Modell hat seinen Ursprung in der Informationstheorie und ist sehr technisch geprägt.

Shannon und Weaver (1949) interessiert in ihrem Modell nicht die Bedeutung einer Mitteilung, sondern Übertragung, Empfang sowie eventuelle Störungen. Sie schreiben daher im Kontext von Kommunikation auch nicht von „messages“ = „Mitteilungen“, sondern von „signals“ = „Signalen“. Bei Shannon und Weaver (1949) ist der Ausgangspunkt für Kommunikationsprozesse die Informationsquelle (Sender). Diese:r wählt eine Nachricht aus, übermittelt wird sie mit Hilfe eines Sendegeräts (Kodierer) in Form von Signalen.

Die Signale werden an den Empfänger übertragen und von den Adressierten mit Hilfe eines Empfangsgeräts (Dekodierer) aufgenommen und letztendlich entschlüsselt. Störungen, egal in welcher Form, bezeichnen Shannon und Weaver als Rauschen. „Allgemein gesprochen muss für erfolgreiche Übermittlung einer Nachricht beidseitige Aufmerksamkeit gegeben sein und die Mitteilung sollte in vorhandenes Wissen integrierbar sein“ (Röhner & Schütz, 2012, S. 18). Voraussetzung für das Gelingen von Kommunikation ist zudem ein teilweise identisches Zeichen- und Bedeutungswissen, wie eine gemeinsame Sprache (ebenda).

### Das Kommunikationsquadrat von Schulz von Thun

Ein weiteres bekanntes Modell, mit stärkerem Bezug zur Praxis, ist das sog. Vier-Seiten-Modell, oder auch Kommunikationsquadrat, von Schulz von Thun, erstmals erwähnt 1977 (Schulz von Thun, 2010, S. 7). „Es eignet sich sowohl zur Analyse konkreter Mitteilungen und zur Aufdeckung einer Vielzahl von Kommunikationsstörungen als auch zur Gliederung des Problemfeldes insgesamt“ (ebenda). Dargestellt wird das Modell in der Abbildung auf der nächsten Seite.

Nach diesem Modell enthält eine Mitteilung, die durch Kommunikation über- tragen wird, vier Komponenten:

- **Sachaspekt, bzw. Sachinformation:** Worüber wird informiert? Sind die Informationen wahr oder unwahr? Relevant oder irrelevant? Hinlänglich oder unzureichend? (Schulz von Thun, 2009).
- **Selbstkundgabe:** „Wenn jemand etwas von sich gibt, gibt er auch etwas von sich“ (Schulz von Thun, 2010, S. 8). Gemeint ist damit der Teil der Persönlichkeit, der jeder Mitteilung vom Sender angeheftet wird.
- **Appell:** Durch Kommunikation wird vom Sender versucht, etwas beim Empfänger zu erreichen, wobei offene und verdeckte Appelle, die sozusagen „zwischen den Zeilen“ übertragen werden, unterschieden werden können.
- **Beziehungshinweis:** Kommunikation beinhaltet immer auch Beziehungs- hinweise, also wie der Sender mit dem Empfänger einer Mitteilung verbunden ist. Ausgedrückt werden diese beispielsweise durch „Formulierung, Tonfall, Mimik und Gestik“ (Schulz von Thun, 2009).

![kommunikationsquadrat.png](assets/kommunikationsquadrat.png)

*Abbildung: Kommunikationsquadrat. Quelle: Schulz von Thun (2009). Die Grafik steht nicht unter einer freien Lizenz!*

Wie in der Abbildung dargestellt spielen die vier Seiten nicht nur bei der Encodierung durch den Sender eine Rolle, sondern auch bei der Decodierung durch den Empfänger (Boss, 2008, S. 22).

Ursprünglich beschreibt das Modell klassische face-to-face-Kommunikation. Relevant ist es jedoch auch für die Analyse von Störungen in der Online-Kommunikation, hier vor allem die Aspekte Appell und Beziehungshinweis:

Bei Online-Formaten ist dringend darauf zu achten, dass die Kommunikation „zwischen den Zeilen“ in der Regel nicht funktioniert, auch Tonfall, Mimik und Gestik kommen nur sehr eingeschränkt an.

Dies bitte im Hinterkopf behalten, da es sonst in Onlineveranstaltungen sehr schnell zu unschönen Missverständnissen kommen kann auf Grund dieser Aspekte, die im digitalen anders funktionieren als im physischen Raum!

## Besonderheiten computervermittelter Kommunikation

Nachdem im vorangegangenen Abschnitt ausführlich dargestellt wurde, was unter Kommunikation verstanden werden soll, ist das Ziel des folgenden Kapitels herauszuarbeiten und zu diskutieren, was die Besonderheiten computervermittelter Kommunikation sind.

Während in den Basisformen von Kommunikation, also face-to-face oder body-to-body-Situationen, die Kommunizierenden zur gleichen Zeit am gleichen Ort sind, ist bei computervermittelter Kommunikation die räumliche und/oder zeitliche Begrenzung aufgehoben.

Während Kommunikation dadurch befreit wird von zeitlichen und räumlichen Hürden, wird gleichzeitig „die Möglichkeit der sensuellen Rezeption und Interpretation eingeschränkt“ (Rogg, 2003, S. 36).

### Definition: Computervermittelte Kommunikation

In der vorliegenden Literatur findet sich keine einheitliche Definition von CvK. Ganz allgemein kann jede Kommunikation, die über einen Computer läuft, als CvK aufgefasst werden. Döring (2013, S. 424) versteht sie als „interpersonale Kommunikation zwischen Einzelpersonen oder in Gruppen, die über Computernetzwerke vermittelt wird (z. B. Kommunikation per E-Mail, in Online - Foren oder auf Social Networking - Sites)“. Dabei wird jedes Medium, das während der Kommunikation zwischen den Kommunizierenden steht, zum Kommunikationsmedium (Rogg, 2003, S. 36), bei computervermittelter Kommunikation ist folglich der Computer das Kommunikationsmedium.

### Asynchrone und Synchrone Kommunikation

Ein wichtiges Merkmal von Kommunikation ist Interaktivität, also das Prinzip der Wechselseitigkeit. Es wird zwischen asynchroner und synchroner Kommunikation differenziert, wobei sich die Unterscheidung auf das Encodieren des Senders und das Dekodieren des Empfängers bezieht.

**Asynchrone Kommunikation** findet zeitversetzt statt, wobei zeitliche und räumliche Hürden überwunden werden, da sich die Kommunizierenden nicht mehr zur gleichen Zeit am gleichen Ort befinden müssen. Mitteilungen werden vom Sender zum Empfänger geschickt, dieser kann zu einem beliebigen Zeitpunkt darauf reagieren. Der Vorteil liegt darin, dass dieser in Ruhe seine Antwort verfassen kann, der Nachteil ist, dass der Sender nicht auf die aktuelle Situation des Empfängers eingehen kann. Im Gegensatz zu direkter Kommunikation ist dem Sender diese nicht bekannt, in der Zeit zwischen Abschicken und Empfang kann sie sich zudem ändern. Bezogen auf das Kommunikationsquadrat von Schulz von Thun bedeutet dies, dass nicht alle vier Kanäle verfügbar sind. Beispiele für asynchrone CvK sind E-Mails, Mailinglisten, Foren, Newsgroups sowie Weblogs (Beck, 2006, S. 57).

Im Gegensatz dazu findet **synchrone Kommunikation** zeitgleich statt. Beispiele hierfür sind Chats und Videokonferenzen mit Tools wie Skype (Beck, 2006, S. 58). Durch diese Art der Kommunikation werden räumliche Distanzen zwischen Sender und Empfänger überbrückt, beide müssen jedoch zeitgleich anwesend sein. Hierdurch kann dem Sender, im Gegensatz zur asynchronen Kommunikation, die Aufmerksamkeit des Empfängers sicher sein. Bei Videokonferenzen können die Kommunikationsparteien, fast wie bei einer face-to-face-Kommunikation, Mimik, Gestik und Stimmlage des Gegenübers sehen und darauf eingehen und dennoch ist dieser Kanal auf eine subtile Art reduziert. In Chats werden als Kompensation zur Übermittlung von Stimmungen häufig Emojis eingesetzt.

### Kategorien der CvK

Auf Grund der unterschiedlichen Reichweite werden im Zusammenhang mit CvK verschiedene Kategorien unterschieden:

Von **One-To-One-Kommunikation** wird gesprochen, wenn die Kommunikation zwischen zwei Personen stattfindet, die jeweils als Sender und als Empfänger fungieren. Beispiele hierfür sind E-Mails sowie Video- und Internettelefonie. Bei der **One-To-Many-Kommunikation** erreicht ein Sender viele Empfänger, beispielsweise mit einem Blog oder einer Mailingliste. Durch **Many-To-Many- Kommunikation** erreichen viele Sender viele Empfänger, z. B. durch ein Internetforum.

## Vor- und Nachteile von CvK

Ein Vorteil von CvK liegt darin, dass die räumliche und zeitliche Begrenzung für das Zustandekommen von Kommunikation aufgehoben ist. Gleichzeitig kann  von einem Sender eine wesentlich höhere Anzahl an Empfängern erreicht wer- den. Kollaboratives Lernen ist ein weiter Aspekt, der durch CvK deutlich verbessert wird: „Im Gegensatz zum kooperativen Lernen ist kollaboratives Lernen demnach ein stärker aufeinander bezogenes und miteinander verwobenes Lernen, dessen Ziel es ist, dass die Lernenden eine von allen geteilte Auffassung oder Vorstellung von einem Problem konstruieren und über den Lernprozess aufrechterhalten“ (Carell, 2006, S. 22). Die Autorin beschreibt dies als „Ko-Konstruktion von Wissen“, im Mittelpunkt steht die individuelle Arbeit, die schließlich zu einem gemeinsamen Ziel führt.

Eine große Gruppe, die kollaborativ an einer umfangreichen Aufgabe arbeitet, generiert automatisch ein Mehr an Informationen. Boss (2008, S. 37) merkt hierzu an, dass sich dies auch kontraproduktiv auf die Handlungsfähigkeit der Gruppe auswirken kann („information overload“). Um diese zu erhalten ist es wichtig, Informationen zu verdichten, zu strukturieren und zu bündeln.

Eine Besonderheit von (vor allem textbasierter) Cvk kann sein, dass soziale Hinweisreize, vor allem non-verbaler Art, herausgefiltert werden (Mocigemba, 2007, S. 68). Das technikkritische Kanalreduktions-Modell geht von einer Verarmung sowie einem allgemeinen Informations- und Aktionsverlust des zwischenmenschlichen Austauschs aus (Döring, 2013, S. 426). Vor allem für den Sender bedeutet die Kanalreduktion jedoch auch einen Kontrollgewinn, „er kann sichergehen, nur die Informationen über die eigene Person zu verbreiten, die er aussenden möchte [...]“ (Mocigemba, 2007, S. 68). Im Gegensatz zum Kanalreduktions-Modell, das die Abwesenheit einiger Sinneskanäle als Mangel wertet, konzentriert sich das Filter-Modell auf die konkreten Auswirkungen der Reduzierung. „Gerade bei textbasierter medialer Kommunikation werden Angaben sozialer Kategorien wie Geschlecht, Alter, Ethnizität etc. (social cues) z. B. durch Anonymisierung herausgefiltert“ (Döring, 2013, S. 427). Dies kann Vorteile, wie den Abbau von Machtasymmetrien und Vorurteilen bringen, aber auch Nachteile, beispielsweise „Regellosigkeit (Anomie), Egozentrismus, Feindseligkeit“ (ebenda).

Zusammenfassend kann gesagt werden, dass CvK auf der einen Seite zahl- reiche Vorteile bietet, wie etwa ganz neue Möglichkeiten des (kollaborativen) Lernens. Auf der andere Seite stehen aber auch offensichtliche Nachteile, wie die Gefahr einer „Informationsüberflutung“ oder Probleme durch eine Kanalreduktion. Diesen Problemen kann weitgehend entgegengewirkt werden, wenn vor  der Bereitstellung von Apps und Tools gründlich abgewogen wird, welche Medien für welche Aufgabe in Frage kommen. Aus diesem Grund sollen im nächsten Abschnitt Modelle der Passung von Aufgabe und Medium vorgestellt werden.

## Modelle der Passung von Aufgabe und Medium

Für das (erfolgreiche) Gelingen von Kommunikation ist die Passung von Aufgabe und Medium eine wichtige Voraussetzung. In der Literatur findet sich keine einheitliche Theorie zur Mediennutzung, „dazu sind die computervermittelten Kommunikationsformen zu ausdifferenziert“ (Döring, 2013, S. 425).

Verstärkt wird dies durch die Interdisziplinarität des Feldes, es finden sich soziologische, psychologische sowie medien- und sprachwissenschaftliche Theorien und Methoden (ebenda). Gemäß dem „Medienökologischen Rahmenmodell“ von Dö- ring (2013, S. 424), dargestellt in der nächsten Abbildung, lassen sich die gängigen CvK-Modelle in drei Gruppen sortieren: Theorien zur Medienwahl, Theorien zum medialen Kommunikationsverhalten sowie Theorien zu Medienmerkmalen (Dö ring, 2013, S. 425):

![mediennutzung.jpeg](assets/mediennutzung.jpeg)

*Abbildung: Medienökologisches Rahmenmodell. Quelle: Döring (2013, S. 424). Dieses Bild steht nicht unter einer freien Lizenz!*

**Die Presence-Konzepte**

Nach der vorangegangenen Abbildung zählen die sog. presence-Konzepte zu den Theorien zum medialen Kommunikationsverhalten.

Unter social presence wird die soziale Verbundenheit trotz Computervermittlung verstanden. Short, Williams, und Christiie (1976) untersuchten als Erste, wie viel Nähe und Distanz bei der Nutzung verschiedener Medien empfunden wird. Ziel der Forschung war es, zu untersuchen, wie face-to-face-Kommunikation am Besten ersetzt werden kann. „Um den Nutzern in diesen virtuellen Umgebungen das Gefühl von Realität zu vermitteln, ist es wichtig, die vermittelten Interaktionen so lebensnah und attraktiv wie möglich zu gestalten“ (Boss, 2008, S. 28).

Nach Short u. a. (1976) vermitteln Medien in unterschiedlichem Ausmaß das Gefühl von social presence: Am stärksten in face-to-face-Kommunikationen, gefolgt von visuellen und auditiven Medien. Am schwächsten ist sie bei textbasierter Kommunikation. Wichtig zu betonen ist, dass es kein bestes Medium per se gibt, social presence ist stark abhängig vom individuellen Empfinden sowie von der Situation, in der das Medium eingesetzt wird.

Die eher schwache social presence ist vor allem bei asynchronen Bildungsveranstaltungen und dort vor allem zu Beginn zu bedenken. Abhilfe kann hier z. B. eine Videoschaltung ziemlich am Beginn schaffen.

In engem Zusammenhang mit der social presence steht die teaching presence. Diese beschreibt das Gefühl der Verbundenheit zwischen Lehrendem und Lernenden und geht auf Garrison, Anderson, und Archer (1999) zurück. Sie definieren teaching presence als „das Design und die Vorbereitung, die Unterstützung des Lehr-/Lerndiskurses und die Lenkung kognitiver und sozialer Prozesse im Hinblick auf persönlich und erzieherisch wertvolle Lernergebnisse“ (Boss, 2008, S. 32). Wie die Definition zeigt bezieht sich das Konzept nicht nur auf den  unmittelbaren Kommunikationsakt, sondern ausdrücklich auch auf die Vorbereitungsphase.

Von besonderem Interesse ist die Frage nach der teaching presence in E-Learning-Umgebungen, die völlig ohne face-to-face-Kommunikation auskommen müssen: Es ist durch Studien (beispielsweise Carey (2001); Ni (2013)) belegt, dass die Abbruch-Quoten in E-Learning-Umgebungen wesentlich höher sind als in traditionellen. In der Literatur, so auch in den beiden zitierten Studien, wird dies damit erklärt, dass die Beziehungen wie auch die Interaktionen zwischen Lehrendem und Lernendem durch face-to-face-Lernen wesentlich ausgeprägter sind, wodurch ein vorzeitiges Abbrechen unwahrscheinlicher wird. An diesem Punkt setzten die presence-Konzepte an, die der Frage nachgehen, wie Beziehung zwischen den Teilnehmern von Online-Umgebungen verbessert werden können um den Lernerfolg positiv zu beeinflussen.

## Grounding in der Kommunikation

Das sog. Grounding-Konzept gehört zu den Theorien der Medienmerkmale und geht auf Clark und Brennan (1991) zurück. Nach den beiden Autoren benötigt Kommunikation eine gemeinsame Wissensbasis (common ground) die durch Informationsaustausch (grounding) gefestigt wird (Clark & Brennan, 1991, S. 128). Clark und Brennan (1991, S. 141) unterscheiden acht Dimensionen, die für den Aufbau einer gemeinsamen Wissensbasis zwischen Kommunikationspartner:innen förderlich sein können:

- Kopräsenz (=copresence): Die Partner:innen befinden sich in derselben Umgebung.
- Sichtbarkeit (=visibility): Die Partner:innen sehen sich.
- Hörbarkeit (=audibility): Die Partner:innen hören sich.
- Synchronizität (=cotemporality): Unmittelbarer Empfang der Botschaft.
- Simultaneität (=simultaneity): Die Partner:innen können gleichzeitig senden und empfangen.
- Sequenzialität (=sequentiality): Die Reihenfolge der Mitteilungen bleibt erhalten.
- Wiederverwendbarkeit (=reviewability): Botschaften können zu einem späteren Zeitpunkt erneut aufgerufen werden.
- Editierbarkeit (=revisability): Botschaften können vor dem Senden über- arbeitet werden.

Mit Hilfe dieser Kategorien können Lernumgebungen unterschiedlich klassifiziert werden.

## Media Richness Theory

Die Media Richness Theory gehört nach obiger Abbildung zu den Theorien der Medienwahl und besagt, dass „die Informationsverarbeitungsleistung in einem medienvermittelten Kommunikationsprozess zwischen Individuen oder innerhalb einer Gruppe umso höher ist, je besser das Medium eine zur Anforderung der gestellten Aufgabe [...] passende Reichhaltigkeit der vermittelten Informationen gewährleistet“ (Boss, 2008, S. 33).

Reichhaltigkeit meint dabei den „Grad, in dem sie das Verständnis für ein Thema in einem bestimmten Zeitintervall ändern kann“ (ebenda), beeinflusst wird sie dabei von mehreren Faktoren, die die Möglichkeiten der Kommunizierenden beeinflussen: Unmittelbarkeit des Feedbacks, Personalisierbarkeit, mögliche Bandbreite der Sprache (ebenda). Schmitz und Fulk (1991) untersuchen in ihrer Studie Organizational colleagues, media richness, and electronic mail: a test of the social influence model of technology use die mediale Reichhaltigkeit verschiedener Medien. Hierfür wurde die Kommunikation von fünf engen Kommunikationspartner:innen sowie einem/einer Supervisor:in in einem Forschungszentrum ausgewertet. Dabei ergab sich die folgende Abstufung der Reichhaltigkeit: Computerausdruck: 2,5 (SD = 1,3); Text (Maschinenschrift): 3,3 (SD = 1,1); E-Mail (3,5 (SD = 0,9); Text (Handschrift): 3,6 (SD = 0,9); Telefon: 3,8 (SD = 0,8); face-to-face = 4,4 (SD =  0,9).

Die dargestellte Reihenfolge deckt sich mit der Veröffentlichung von Daft und Lengel (1986), den beiden Begründern der Theorie. „Die face-to-face-Kommunikation wird als Königsweg zur Reduktion von Mehrdeutigkeit angesehen“.

## Das Task-Media-Fit-Modell

Nach dem medienökologischen Rahmenmodell (Abbildung 5) ist das Task-Me- dia-Fit-Modell eine Theorie zur Medienwahl. McGrath und Hollingshead (1994) erstellten ein Modell der Aufgabenklassifikation, abgebildet auf der nächsten Seite.

Die optimale Passung liegt, wie eingezeichnet, auf der Diagonalen. Kombinationen rechts oberhalb der Diagonalen weisen eine schlechte Passung auf, da durch das Medium ein Mehr an Informationen übermittelt wird als für die Aufgabe notwendig wäre (Boss, 2008, S. 35). Bei Kombinationen unterhalb der Diagonalen verhält es sich konträr, es werden zu wenige Informationen durch das Medium übermittelt.

![taskmedia.jpeg](assets/taskmedia.jpeg)

*Abbildung: Das Task - Media - Fit - Modell von McGrath und Hollingshead (1994, S. 111). Abbildung von: Boss (2008, S. 35). Die Abbildung steht nicht unter einer freien Lizenz!*

## Media Synchronicity Theory

Die Media Synchronicity Theory geht auf Dennis und Valacich (1999) zurück und kann als alternatives Konzept zur Media Richness Theory verstanden wer- den. Sie setzt dort an, wo diese ihre Schwachstellen hat: Dennis und Valacich (1999) kritisieren vor allem, dass Reichhaltigkeit (=Richness) zu sehr an die so- ziale Präsenz (=social presence) geknüpft sei (Dennis & Valacich, 1999, S. 2). Die beiden Autoren hingegen definieren mediale Reichhaltigkeit zum einen durch eine hohe social presence, zum anderen durch die Kapazität, welches Maß an Informationen theoretisch übertragen werden kann.

Die Media Synchronicity Theory unterscheidet dabei zwei Teilprozesse: Die Informationsübermittlung ist „das Sammeln von Fakten aus unterschiedlichen Quellen“ (Boss, 2008, S. 36), die Informationsverdichtung ist „das Zusammenführen dieser Informationen, um zu einer (gemeinsamen) Interpretation der Informationen zu gelangen. Ein weiterer Kritikpunkt ist, dass die Media Richness Theory nicht zwischen der Kommunikation zwischen zwei Personen und zwischen Gruppenkommunikation unterscheidet.

Dennis und Valacich (1999, S. 2) unterscheiden zunächst fünf Merkmale, die sich in allen Medien finden:

- Unmittelbarkeit des Feedbacks: Möglichkeit des schnellen Feedbacks und Voraussetzung für schnelle bidirektionale Kommunikation.
- Varietät der Symbole: Anzahl der Möglichkeiten mit denen Informationen übermittelt werden können.
- Parallelität: Anzahl der (sinnvoll) möglichen Konversationen.
- Überarbeitbarkeit: Die Möglichkeiten der Sender:in, die Mitteilung vor dem Absenden zu überarbeiten und zu verbessern.
- Wiederverwendbarkeit: Möglichkeit der Weiterverarbeitung einer Konversation zu einem späteren Zeitpunkt.
- Dennis und Valacich (1999, S. 3) betonen, dass ein Medium nicht eindeutig bewertet werden darf, sondern im jeweiligen Kontext gesehen werden muss. Es gibt kein „Medium der größten Reichhaltigkeit“ (Dennis & Valacich, 1999, S. 1).

## Zusammenfassung

Abschließend soll tabellarisch gezeigt werden, wie die vorgestellten Theorien genutzt werden können, um digitale Bildungsangebote zu analysieren:

![mediennutzung.png](assets/mediennutzung.png)

*Abbildung: Beispielhafte Anwendung der Kriterien verschiedener Mediennutzungstheorien auf Zoom und moodle. Eigene Darstellung. CC BY-SA Christian Pfliegel*

Die Tabelle zeigt sehr schön, welche Kanäle jeweils angesprochen werden und welche mediale Reichhaltigkeit möglich ist. Eine gelungene, digitale Bildungsveranstaltung sollte das Ziel haben, den Teilnehmer:innen sowohl bezgl. angesprochener Kanäle, als auch bzgl. Reichhaltigkeit und Synchronizität möglichst viele Freiheiten, aber auch möglichst viel Abwechslung zu bieten.