# Freie Software und Services für Bildung

#### Abstract

Manchmal reichen *Zoom* und *Moodle* nicht aus um gute digitale Bildung anzubieten oder zu produzieren. Vielleicht möchtest du ein Erklärvideo erstellen oder ein Arbeitsblatt mit einer Grafik verschönern? Vielleicht wird auch ein gemeinsames Dokument benötigt oder ein „kurzer Link"? Im folgenden Beitrag möchte ich ein paar offene und kostenlose Tools und Services vorstellen, mit denen ich in den letzten Jahren immer wieder gerne gearbeitet habe und die mir in verschiedenen Kontexten gute Dienste geleistet haben.

#### Handbrake

Handbrake ([www.handbrake.fr](http://www.handbrake.fr)) ist ein offener, freier und kostenloser Video- Konverter, der auch noch leicht zu bedienen ist. Verfügbar ist Handbrake für alle gängigen Betriebssysteme: Linux, macOS, Microsoft Windows. Die Stärke von Handbrake ist, dass damit eine Konvertierung fast jedes Videoformats möglich ist. Zudem kann die Auflösung eures Videos angepasst werden sowie eine Web- Optimierung vorgenommen werden:

![handbrake.png](assets/handbrake.png)

*Screenshot: Handbrake. Alle Rechte bei [handbrake.fr](http://handbrake.fr). Bearbeitung: CC BY-SA Christian Pfliegel*

Aber was hat ein Video-Konverter wie *Handbrake* mit digitaler Bildung zu tun?

Es gibt gute Gründe, Vorträge von Referent:innen im Vorfeld aufzuzeichnen und diese dann bei der Veranstaltung per Bildschirmfreigabe zu zeigen: Zum einen ist es sicherer, da der Vortrag nicht ausfallen muss, sollte die Verbindung zum dem/der Vortragenden abreißen. Zum anderen ist genau planbar, wie lange der Vortrag dauert und ihr als Veranstaltender habt eine kurze Pause, die ggfs. für Absprachen genutzt werden kann.

Häufig sind Aufzeichnungen, die ihr zugeschickt bekommt, entweder in einem Format, das nicht optimal auf eurem Rechner läuft oder ihre Auflösung ist zu hoch und die Datei damit zu groß für eine Einspielung per Bildschirmfreigabe. In diesem Fall bietet es sich an, die Aufzeichnung vorher mit Hilfe von Handbrake zu optimieren.

In der Praxis haben sich (bei einer durchschnittlichen Bandbreite von 20 bis 100) folgende Einstellungen für eine Bildschirmfreigabe bewährt:

- Format: mp4
- Web-Optimiert: ja

Voreinstellung: General => Fast 720p30

Mit dieser Voreinstellung erhaltet ihr ein Video, das nicht zu groß für eine Übertragung ist, das aber dennoch ausreichende Bildqualität auch bei angezeigten Texten oder Folien liefert.

#### OBS = Open Broadcast Studio

Open Broadcast Studio, oder kurz OBS, ist ein Programm, aus dem zum einen direkt gestreamt werden kann (YouTube, Vimeo, Twitch und viele weitere), zum anderen kann damit auch lokal aufgezeichnet werden. Der Vorteil: OBS ist Open-Source und auf allen gängigen Desktop-Betriebssystemen verfügbar. Der riesige Funktionsumfang kann dabei noch durch Plugins erweitert werden. Für den Kontext „Digitale Bildung“ finde ich vor allem die Screencast-Funktion (= parallele Aufzeichnung des Bildschirms + Audiospur) spannend: Mit OBS ist es sehr einfach, alleine einen Vortrag inklusive Präsentation, ein Kamerabild des Vortragenden und eine zugehörige Audiospur in einem Schritt aufzunehmen, ohne dass eine komplexe und langwierige Nachbearbeitung notwendig wird.

Die Bedienung ist dabei recht schnell zu erlernen: Die Zusammenstellung von Videoquelle, Audioquelle, Präsentation usw. wird über sog. Szenen vorgenommen, wobei jede Szene eine Zusammenstellung verschiedener Quellen ist:

![obs.png](assets/obs.png)

*Die Oberfläche von OBS. Eigener Screenshot. Alle Rechte bei OBS*

Auf der linken Seite kannst du dir deine Zusammenstellung der Quellen (Bildschirmansicht, Präsentation, Videoquellen, Audio, Text usw.) anschauen, auf der rechten Seite siehst du die eigentliche Aufnahme.

Der Wechsel von Vorschau auf Aufnahme (bzw. Programm) erfolgt mittels des Schiebereglers in der Mitte. Rechts unten wird der Stream bzw. die Aufnahme gestartet, zudem können hier die Einstellungen vorgenommen werden. Folgende Einstellungen haben sich für Aufzeichnungen, die für Online-Veranstaltungen erstellt werden, bewährt:

- Videobitrate: 1000 bis 1200
- Videoformat: mp4
- Videoqualität: gleich wie Stream
- Basis-Video-Auflösung (unter dem Menüpunkt Video): 1280 x 720 - Frames-per-second (FPS): 30

Da OBS ein sehr verbreitetes Programm ist finden sich beispielsweise bei YouTube zahlreiche empfehlenswerte Tutorials in unterschiedlicher Ausführlichkeit.

#### Freie Software-Onlinedienste: Chatons

Manchmal kann es im Rahmen von (digitalen) Bildungsveranstaltungen sehr praktisch sein, auf die Schnelle ein Etherpad anzulegen, einen Termin zu finden oder daten- sicher Fotos oder Dateien zu sammeln. Sehr hilfreich bei all diesen Aufgaben ist die Seite Chatons ([https://entraide.chatons.org/de/](https://entraide.chatons.org/de/)).

Hier können zahlreiche freie Angebote direkt und ohne Vorkenntnisse gestartet werden:

![chatons.png](assets/chatons.png)

*Chatons. Eigener Screenshot. Alle Rechte bei Chatons.*

Die Möglichkeiten der Seite sind im Grund selbsterklärend. Ein Blick lohnt sich vor allem in der Vorbereitungsphase einer Online-Veranstaltung, da die Angebote eventuell gut in das Programm eingebunden werden können.

#### Freie Bilder: Freepik, Cocomaterial, Pixabay und Unsplash

Du benötigst eine Grafik oder eine Darstellung zur Verschönerung eines Arbeitsblatts oder einer Online-Umgebung? In diesem Fall lohnt sich immer ein Blick auf diese Webseiten zu werfen:

- [www.freepik.com](www.freepik.com)
- [www.cocomaterial.com](www.cocomaterial.com)
- [www.pixabay.com](www.pixabay.com)
- [www.unsplash.com](https://unsplash.com)
- [https://commons.wikimedia.org/](https://commons.wikimedia.org/wiki/Main_Page)
- [https://search.openverse.engineering/](https://search.openverse.engineering/)

> **Achtung: Manchmal ändern sich die Lizenz-Bestimmungen der Seiten, manchmal bieten "schwarze Schafe" Bilder an, die gar nicht unter auf einer der Plattformen auftauchen dürften. Es ist daher sinnvoll ganz genau hinzusehen, bevor ein Bild verwendet wird. Vor allem bei größeren Publikationen ist es oft besser eigene Bilder zu produzieren oder eine Lizenz für ein Bild zu kaufen!**

**Der besondere Tipp für freie Bilder:**

Ganz rechtssicher dürfen Bilder verwendet werden, die nicht von einem Menschen sondern von einer künstlichen Intelligenz erstellt werden. Ein sehr guter Tipp hierzu findet sich bei den Kollegen von rpi-virtuell:

[https://blogs.rpi-virtuell.de/digital/2019/10/03/kostenlose-ai-fotos/](https://blogs.rpi-virtuell.de/digital/2019/10/03/kostenlose-ai-fotos/)


#### OERhoernchen

Zum Abschluss dieses Beitrags noch ein Tipp um gute OER (Open Educational Resources) zu finden: das OERHoernchen ([oerhoernchen.de](oerhoernchen.de)). „Mit der OERhörnchen Bildungsmaterialsuche können gezielt Lehr-/Lernmaterialien von ausgewählten Bildungsprojekten gefunden werden, welche durch eine freie Lizenz legal nutz- und veränderbar sind.“