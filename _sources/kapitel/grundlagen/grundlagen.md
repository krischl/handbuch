# Grundlagen

### Theoretische Grundlagen von Bildung in der Digitalität

Bevor in den nächsten Kapiteln praktische Tipps für die Gestaltung digitaler Bildungsformate folgen, ist es sinnvoll, einen kurzen Exkurs in die Theorie des digitalen Lernens zu unternehmen: Viele denken bei dem Begriff digitale Bildungsformate zunächst an langweilige E-Learnings, durch die man sich zwangsweise alleine, ohne persönlichen Kontakt zu anderen, gelangweilt und vielleicht nebenbei durch klickt . . . im schlimmsten Fall noch mit Technik, die nicht richtig funktioniert. Oder aber an stundenlange, langweilige Zoom-Meetings. Dieses Handbuch wird zeigen, dass digitale Bildungsformate anders sind als traditionelle, aber ebenso Spaß machen und bereichernd sein können.

### Digitales Lernen

Was ist das überhaupt, dieses digitale Lernen? Unter dem Begriff digitales Lernen oder digitale Bildungsformate werden in diesem Handbuch alle Formen des elektronischen Lernens verstanden werden. Online und Offline. Der Vorteil der digitalen Formate ist eine Erweiterung der Möglichkeiten, da sie ein gewisses Maß an Orts- und Zeitunabhängigkeit mit sich bringen. Aber auch die Nachteile müssen genannt werden:

- Digitale Formate sind nicht für jeden Zweck geeignet: Für die Vermittlung von formellen Inhalten eignen sie sich in der Regel recht gut, alles Informelle, sowie der Aufbau von Netzwerken oder Beziehungen ist deutlich schwerer!
- Digitale Formate sind in der Erstellung teilweise kosten- und zeitintensiv. Vor allem bei kleineren, speziellen Zielgruppen muss der Aufwand mit dem Nutzen in Relation gesetzt werden.
- Digitale Formate sind anders, was eine Einarbeitung in vielen Fällen notwendig macht.

### Digitales Lernen - Traditionelles Lernen

Das Lernverständnis in digitalen Formaten ist ein anderes als das in traditionellen Formaten, zusammengefasst in der Abbildung von Lisa Rosa ([https://twitter.com/lisarosa](https://twitter.com/lisarosa)):

![2. Lernverständnis.jpeg](assets/lernen.jpeg)

*Quelle: CC-BY Lisa Rosa Link: [https://shiftingschool.wordpress.com/2017/11/28/lernen-im-digitalen-zeitalter/](https://shiftingschool.wordpress.com/2017/11/28/lernen-im-digitalen-zeitalter/)*

Es ist wichtig, sich dieses andere Bild von Lernen vor der Konzeption einer eigenen Bildungsveranstaltung zu verinnerlichen und zu verstehen. Ein häufiger Grund für das Scheitern digitaler Formate ist die 1:1-Übertragung von traditionellen Settings auf ein digitales Setting: Ein Seminar, das online genauso umgesetzt werden soll wie ein vorher geplantes Präsenzseminar funktioniert (in aller Regel) nicht!

### Arten des digitalen Lernens

Beim digitalen Lernen können synchrone (= zur gleichen Zeit am Rechner) und asynchrone (= zu beliebiger Zeit am Rechner) Formen unterschieden werden, wobei es kein besser oder schlechter, sondern nur ein passend/unpassend gibt:

![3. asynchron - synchron.jpeg](assets/asynchron.jpeg)
*Quelle: Marc Seegers (twitter: @seegersmc)*

Die Form muss bei der Konzeption des digitalen Bildungsformats von Anfang an mit bedacht werden und ist von verschiedenen Faktoren abhängig:

- Besteht die Möglichkeit, dass alle Teilnehmer:innen synchron, also zur gleichen Zeit am Rechner sind? Dies ist u. a. abhängig von der Stabilität der Internetverbindung des Ortes, von dem aus teilgenommen werden soll. Aber auch Zeitzonen, der Terminkalender der Teilnehmer:innen usw. haben direkten Einfluss.
- Welcher Zeitrahmen ist für die Bildungsveranstaltung angesetzt? Für asynchrone Formate wird in der Regel mehr Zeit benötigt, da die Interaktionen zeitlich nicht direkt aufeinander bezogen sind. Die Teilnehmer:innen müssten ansonsten die Plattform 24 Stunden am Tag im Auge behalten.
- Wie ist die technische Ausstattung der Veranstalter:innen und der Teilnehmer:innen? Synchrone Formate benötigen bessere Technik als asynchrone Formate, für die häufig ein Webbrowser reicht.

### Formate von Bildungsangeboten

Im Bereich des digitalen Lernens können folgende Formate unterschieden werden, die jeweils unterschiedlich geplant und konzipiert werden müssen:

#### Online-Veranstaltungen

Online-Veranstaltungen finden sehr oft synchron statt, alle Teilnehmer:innen sitzen dabei zur gleichen Zeit vor dem Rechner. Zum Einsatz kommen dabei Videokonferenztools wie Adobe Connect, Zoom, Jitsi, GoToMeeting usw. Neben dem Austausch per Video kommen oft kollaborative Möglichkeiten, wie Pinnwände, Padlets oder ähnliches zum Einsatz.

#### Offline- oder Präsenz-Seminare: das klassische, analoge Format

Alle Teilnehmer:innen treffen sich an einem Ort. Digital unterstützt werden kann dieses Format beispielsweise mit Feedbacktools wie Mentimeter ([www.mentimeter.com](http://www.mentimeter.com)) oder auch Plickers ([www.plickers.com](http://www.plickers.com)). Der Vorteil von Plickers ist, dass die Teilnehmer:innen keine Technik für die Nutzung benötigen - das Tool ist auf jeden Fall einen Blick wert!

#### Blended Learning

Beim Format des blended learning handelt es sich um eine Mischform aus Online- und Präsenz: Beide Phasen finden im Wechsel statt und ergänzen sich.

Geeignet ist diese Form z. B. für Sprachkurse: Nach einem Wochenende in Präsenz folgt eine Onlinephase, die auf die nächste Präsenzphase vorbereitet und beispielsweise mit Vokabel- oder Grammatikübungen gefüllt sein kann.

#### Hybride Formate

Im Unterschied zum blended learning finden bei hybriden Seminaren die Präsenz- und Online-Phasen nicht nacheinander statt, sondern gleichzeitig: Ein Teil der Teilnehmer:innen ist vor Ort, ein Teil ist online zugeschaltet. Dieses Format eignet sich, wenn nicht alle Teilnehmer:innen vor Ort teilnehmen können, da sie entweder zuweit entfernt sind oder weil es mehr Interessierte gibt als in den Raum dürfen.

### ADDIE-Modell

Sehr hilfreich für die Konzeption eines digitalen Bildungsformats ist die Nutzung des sog. ADDIE-Modells. Das ADDIE-Modell ist ein im Bereich der Planung und Konzeption von Lehrumgebungen eingesetztes Instructional-System-Design-Modell, dessen Kern eine systematische Koordination der Entwicklungsphasen Analyse (=Analyze), Konzeption (=Design), Entwicklung im engeren Sinne (=Develop), Implementierung (=Implement) und Evaluation (=Evaluate) ist.

Das **ADDIE-Modell** besteht aus folgenden (Teil-)schritten:

![addie.jpeg](assets/addie.jpeg)
*Abbildung: Das ADDIE-Modell. CC BY-SA Christian Pfliegel*

- **Analyse:** Vor Beginn der eigentlichen Konzeption ist zu ermitteln, welchen Zweck oder Bedarf das Projekt erfüllen soll und wer die Adressat:innen sind. Didaktische Mängel in Lehrveranstaltungen sind häufig auf defizitäre Analysen der Zielgruppe zurückzuführen.
- **Konzeption:** In dieser Phase werden die Lehrziele bereits so verbindlich formuliert, dass diese später sehr konkret überprüft werden können.
- **Entwicklung im engeren Sinne:** In diesem Schritt werden die Inhalte und Medien produziert und die benötigte Plattform wird angelegt.
- **Implementierung:** Die entwickelten Inhalte, Materialien und Medien werden in konkreten Bildungskontexten eingesetzt, bzw. eingeführt und etabliert. Dieser Schritt ist die eigentliche Durchführung des Projekts.
- **Evaluation:** Ein wichtiger Schritt ist die abschließende Evaluation des Projekts. Diese dient zum einen dazu, Probleme in der praktischen Umsetzung zu identifizieren und zum anderen der Messung, ob die Lehrziele, die in der Konzeptionsphase festgelegt wurden, überhaupt erreicht sind. Eine Besonderheit des ADDIE-Modells ist, dass nach jedem Entwicklungsschritt eine Evaluation erfolgt, die dann in die weitere Konzeption einfließt.

### Was ist gutes digitales Lernen?

Was macht gutes, digitales Lernen überhaupt aus? Um diese Frage zu beantworten, bietet sich eine Hilfestellung aus dem eBildungslabor ([www.ebildungslabor.de](http://www.ebildungslabor.de)) an, die unter dem Titel „4-Felder-Analyse für gutes E-Learning“ unter einer CC BY-SA Lizenz von Nele Hirsch veröffentlicht wurde und die ich für das vorliegende Buch geremixt habe.

Damit digitales Lernen gelingen kann sind im Vorfeld, analog zu normalen Unterricht, einige Analyseschritte notwendig:

- Lehrziele: Welchen Bildungsbedarf soll das E-Learning-Projekt abdecken? Was sollen die Teilnehmer:innen am Ende der Bildungsmaßnahme wissen/können?
- Zielgruppe: Wie groß ist die Zielgruppe? Welche Motivation bringt die Zielgruppe mit? Inwieweit bestehen Vorkenntnisse zum Thema? Welche besonderen Bedingungen, z. B. Barrierefreiheit, müssen beachtet werden? Wie ist die Medienkompetenz der Zielgruppe einzuschätzen? Wie ist die Zielgruppe technisch ausgestattet?
- Bedingungen: Wie viel Zeit haben wir für die Entwicklung und Durchführung? Welche Personen können welche Kompetenzen beisteuern (technisches Wissen, Didaktik, Datenschutz)? Wie viele (vor allem zeitliche) Ressourcen haben wir für die Betreuung?
- Nachhaltigkeit: Wie viel Zeit haben wir für die Entwicklung und Durchführung? Welche Personen können welche Kompetenzen beisteuern (technisches Wissen, Didaktik, Datenschutz)? Wie viele (vor allem zeitliche) Ressourcen haben wir für die Betreuung?

Die Schwierigkeit liegt darin, dass beim digitalen Lehren und Lernen sowie bei Online-Veranstaltungen verschiedene Ebenen wirken, die sich gegenseitig beeinflussen und die geschickt miteinander kombiniert werden müssen.

Die folgende Abbildung zeigt, dass bei der Konzeption von Online-Veranstaltungen sowohl **technisches Wissen** als auch **pädagogisches** und **inhaltliches** Wissen einfließen müssen:

![5. TPACK-new.png](assets/tpack.png)

*TPACK-Modell, Darstellung: CC0*

Das vorliegende Handbuch will anhand von Beispielen aus der Praxis zeigen, wie diese Bereiche geschickt kombiniert werden können.

> **Praxistipp:** Ein interessanter Ansatz für die Gestaltung guten Lernens sind die 4Ks oder auch die 4K-Kompetenzen. Diese umfassen: kritisches Denken, Kreativität, Kommunikation und Kollaboration. Zur Vertiefung ist dieser Blog-Eintrag von Jöran Muuss-Merholz sehr zu empfehlen:
[https://www.joeran.de/die-4k-skills-was-meint-kreativitaet-kritisches-denken-kollaboration-kommunikation/](https://www.joeran.de/die-4k-skills-was-meint-kreativitaet-kritisches-denken-kollaboration-kommunikation/)